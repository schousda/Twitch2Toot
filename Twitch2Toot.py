import json
import requests
from datetime import datetime, date, time, timedelta
from rich import print
from rich.console import Console
from rich.prompt import Confirm, Prompt
from mastodon import Mastodon

console = Console()

#read configuration file
def get_parameters(fname):

    twitch_client_id = None
    twitch_client_secret = None
    twitch_creator = None

    mastodon_instance = None
    mastoton_client_id = None
    mastodon_client_secret = None
    mastodon_access_token = None

    config_file = open_file(fname, "r")
    if config_file is not None:
        configuration = json.load(config_file)
        try:
            twitch_parms = configuration['twitch']
            mastodon_parms = configuration['mastodon']

            twitch_client_id = twitch_parms[0]['client_id']
            twitch_client_secret = twitch_parms[0]['client_secret']

            config_file.close()
        except KeyError:
            console.print("config.json could not be read. Please make sure it is formatted correctly.\n", style="bold red")
            config_file.close()
            config_file = None

    if config_file is None:
        console.print("No configuration file found!\n", style="bold red")

    return configuration

#authenticate to twitch
def auth_twitch(auth):
    oauth_token = None
    client_id = (auth['client_id'])
    client_secret = (auth['client_secret'])
    oauth_token = client_creds_auth(client_id, client_secret)
    if oauth_token is None:
        console.print("Your Twitch credentials are invalid. Please try again.\n", style="bold red")
    if oauth_token is not None:
        console.print("Authentication successful!\n", style="bold green")
    return client_id, oauth_token


# Obtain oauth token with client_id and client_secret
def client_creds_auth(client_id, client_secret):
    params = {'client_id': client_id, 'client_secret': client_secret, 'grant_type': 'client_credentials'}
    r = requests.post(url = "https://id.twitch.tv/oauth2/token", params = params)
    print(r.status_code)
    if r.status_code == 200:
        return r.json()['access_token']
    else:
        return None

# Get broadcaster_id from broadcaster_name
def get_broadcaster_id(twitch, oauth_token):
    headers = {'Authorization': 'Bearer ' + oauth_token, 'Client-Id': twitch['client_id']}
    params = {'login': str(twitch['broadcaster'])}
    r = requests.get(url = "https://api.twitch.tv/helix/users", params = params, headers = headers)
    if not r.json()['data']:
        return None
    else:
        return str(r.json()['data'][0]['id'])

#get start and end date of clips
def get_dates(twitch):
    end_date = datetime.now()
    time_diff = timedelta(hours=float(twitch['max_hours']))
    if twitch['last_run']:
        try:
            last_run = datetime.strptime(twitch['last_run'], "%Y-%m-%dT%H:%M:%SZ")
            last_run = end_date - last_run
        except ValueError:
            raise ValueError("Incorrect data format, should be %Y-%m-%dT%H:%M:%SZ")
        if last_run < time_diff:
            time_diff = last_run

    start_date = end_date - time_diff
    start_date = start_date.strftime("%Y-%m-%dT%H:%M:%SZ")
    end_date = end_date.strftime("%Y-%m-%dT%H:%M:%SZ")
    return start_date, end_date

# Get game_id from game_name
def get_game_id(twitch, oauth_token):
    headers = {'Authorization': 'Bearer ' + oauth_token, 'Client-Id': twitch['client_id']}
    params = {'name': twitch['game']}
    r = requests.get(url = "https://api.twitch.tv/helix/games", params = params, headers = headers)
    if not (r.json()['data']):
        console.print("Game not found. Please enter the [bold]exact[/bold] title of the game.", style="red")
        return None
    else:
        returned_game = r.json()['data'][0]
        return returned_game['id']

# Iterative function to retrieve all clips with specified filters
def retrieve_clips(twitch, oauth_token, broadcaster_id, date_range):
    cursor = ""
    clips = []
    started_at = date_range[0]
    ended_at = date_range[1]
    print(started_at)
    print(ended_at)

    headers = {'Authorization': 'Bearer ' + oauth_token, 'Client-Id': twitch['client_id']}
    params = {'broadcaster_id': broadcaster_id, 'first': 100, 'started_at': started_at, "ended_at": ended_at}
    #params = {'broadcaster_id': broadcaster_id, 'first': 100}
    if cursor: params['after'] = cursor

    while cursor is not None:
        r = requests.get(url = "https://api.twitch.tv/helix/clips", params = params, headers = headers)
        # If no clips are found
        try:
            clips += r.json()['data']
        except KeyError:
            return clips
        # Once we reach the end of the clips
        try:
            cursor = r.json()['pagination']['cursor']
            params['after'] = cursor
        except KeyError:
            cursor = None
    
    return clips

# Modify the clips list to match provided filters
def filter_clips(clips, creator_filter='', title_filter='', game_filter=''):
    for clip in clips[:]:
        if (creator_filter and clip['creator_name'].casefold() != creator_filter.casefold()
            or game_filter and clip['game_id'] != game_filter
            or title_filter and title_filter.casefold() not in clip['title'].casefold()):
            clips.remove(clip)
    return clips

# Sort clips by specified order before we write them to file
def sort_clips(clips, order='newest'):
    match order:
        case "oldest":
            clips.sort(key = lambda clip: datetime.strptime(clip['created_at'], "%Y-%m-%dT%H:%M:%SZ"))
        case "newest":
            clips.sort(key = lambda clip: datetime.strptime(clip['created_at'], "%Y-%m-%dT%H:%M:%SZ"), reverse=True)
        case "unpopular":
            clips.reverse()
    return clips

# Attempt to open file before accessing to ensure we have permission
def open_file(fname, perms):
    try:
        file = open(fname, perms)
        return file
    except OSError:
        return None

# authenticate to mastodon
def connect_mastodon(mastodon):
    if not mastodon['instance_url'].startswith(('http://', 'https://')):
        mastodon['instance_url'] = 'https://' + mastodon['instance_url']
    mastodon_api = Mastodon(
        api_base_url=mastodon['instance_url'],
        client_id=mastodon['client_id'],
        client_secret=mastodon['client_secret'],
        access_token=mastodon['access_token']
        )
    return mastodon_api

# post to mastodon
def post_mastodon(mastodon, mastodon_api, clips):
    for clip in clips:
        mastodon_api.status_post(mastodon['template'].format(**clip)[:499])
        #print(mastodon['template'])
        print(mastodon['template'].format(**clip)[:499])
    return None

# update config file with time of last run
def update_config_file(fname, end_date):
    config_file = open_file(fname, "r")
    if config_file is not None:
        configuration = json.load(config_file)
    configuration['twitch'][0]['last_run'] = end_date
    config_file = open_file(fname, "w")
    if config_file is not None:
        config_file.write(json.dumps(configuration, indent=3))
        config_file.close()
    


if __name__ == "__main__":
    fname = 'config.json'
    config = get_parameters(fname)
    print(config)
    oauth_token = auth_twitch(config['twitch'][0])[1]
    print(oauth_token)
    broadcaster_id = get_broadcaster_id(config['twitch'][0], oauth_token)
    print(broadcaster_id)
    date_range = get_dates(config['twitch'][0])
    print(date_range)
    game_id = ""
    if config['twitch'][0]['game']:
        game_id = get_game_id(config['twitch'][0], oauth_token)
        print(game_id)
    clips = retrieve_clips(config['twitch'][0], oauth_token, broadcaster_id, date_range)
    if game_id or config['twitch'][0]['creator'] or config['twitch'][0]['title']:
        clips = filter_clips(clips, config['twitch'][0]['creator'], config['twitch'][0]['title'], game_id)
    if clips is not None:
        clips = sort_clips(clips, config['twitch'][0]['order'])
        mastodon_api = connect_mastodon(config['mastodon'][0])
        mastodon_post = post_mastodon(config['mastodon'][0], mastodon_api, clips)
    update_config_file(fname, date_range[1])