# Twitch2Toot 

This Python script toots Twitch clips to Mastodon.

# Installation
Clone this repository or download and unzip the source code. Open your computer's terminal in the folder created. Ensure Python 3.10 or later is installed (`python3 -v`). Run the following:

`python3 -m pip install -r requirements.txt`  

# Configuration
Rename the config_examples.json to config.json
After this you need to add your information to the file.

## Twitch section

Please make sure you have created a [Developer Application on Twitch here](https://dev.twitch.tv/console/apps) and have its client ID and client secret ready, as the tool needs it to authenticate with the Twitch API. 

 - `client_ID`: Add the Twitch client ID here 
 -  `client_secret`: Add the Twitch client secret here
 - `broadcaster`: Add the name of the twitch channel
 - `game`: Add the exact name of a twitch categorie to filter for a specific categorie
 - `creator`: Add the name of the clip creator to filter for a specific clip creator
 - `title`: filter for a specific string in the clip title
 - `order`: Add `"oldest"`, `"newest"`, `"popular"`, `"unpopular"` to define the sort order
 - `max_hours`: Value in hours to limit the maximum time range
 - `last_run`: This field will be updated after each run. The next run will start from this timestamp.

## Mastodon
Please make sure you have created a Mastodon App.
In your account go to *Preferences - Development* and create a new application. After you created the app open the application and get the client key, client secret and your access token. Add this information the the mastodon section.

- `client_id`: Add the Mastodon client ID here 
- `client_secret`: Add the Mastodon client secret here 
- `access_token`: Add the Mastodon access token here 
- `instance_url`: Add your instance URL
- `template`: Here you can specify the text to be tooted. To insert field values of a clip, use curly brackets. Valid fields can be found [here](https://dev.twitch.tv/docs/api/clips#getting-clips).

## Usage
Run the script with `python3 ./Twitch2Toot.py` or use the bash file to run it as a cronjob. Please don't forget to edit the path to your script there.

# Credits
This script is based on the [clipgrabber by aspensykes](https://github.com/aspensykes/clipgrabber)